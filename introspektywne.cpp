#include <iostream>
#include <cmath>
using namespace std;
#define ROZMIAR 10				//rozmiar o 1 wi�kszy, tablica ma pierwszy element(tab[0]) pusty

template <typename T>
void zamien(T &a, T &b){
	T tmp=a;
	a=b;
	b=tmp;
}

int lewy_syn(int k){
	return 2*k;
}

int prawy_syn(int k){
	return 2*k+1;
}

template <typename T>
void tab_na_kopiec (T dane[], int rozmiar, int i){
	int wiekszy;
	if(lewy_syn(i)<=rozmiar && dane[lewy_syn(i)]>dane[i])
		wiekszy=lewy_syn(i);
	else wiekszy=i;
	if(prawy_syn(i)<=rozmiar && dane[prawy_syn(i)]>dane[wiekszy])
		wiekszy=prawy_syn(i);
	if(wiekszy!=i){
		zamien(dane[wiekszy],dane[i]);
		tab_na_kopiec(dane,rozmiar,wiekszy);
	}
}

template <typename T>
void sortowanie_kopiec(T dane[], int rozmiar){
	for(int i=rozmiar/2; i>0; i--)			//zamiana tablicy na sterte
		tab_na_kopiec(dane,rozmiar,i);		//w ktorym najwiekszy element jest ojcem,
											//a dziecmi sa elementy mniejsze co do wartosci
	
	for(int i=rozmiar; i>1; i--){			//w petli tworzenie kopca, za kazdym razem zmniejszajac
											//rozmiar danych, pomijajac w ten sposob za kazdym razem ostatni element
		zamien(dane[i], dane[1]);			//zamiana ostatniej wartosci z pierwsza
		rozmiar--;							
		tab_na_kopiec(dane,rozmiar, 1);		//odtworzenie wlasnosci sterty by utworzyc kopiec
	}
}

//funkcja liczaca mediane
template <typename T>
T mediana(T a, T b, T c){
	T med=max(min(a, b), min(max(a, b), c)); //zwraca element srodkowy zbioru 3 liczb
}
//funkcja wybierajaca pivota i dzielaca tablice na dwie podtablice- elementy mniejsze i wieksze od pivota
template <typename T>
T partycjonowanie(T dane[], int poczatek, int koniec){
	T pivot=mediana(dane[poczatek], dane[koniec], dane[(poczatek+koniec)/2]);		//pivot wybierany jako mediana elementow pierwszego,
	int i=poczatek;																	//srodkowego i ostatniego
    int j=koniec;
    T temp;																		//dana tymczasowa do zamiany dwoch elementow na tablicy
    do
    {
		 while (pivot<dane[j])    j--;							//najpierw wyszukujemy pierwszy element z prawej podtablicy ktory jest mniejszy od pivota
         while (pivot>dane[i])    i++;							//nastepnie wyszukujemy pierwszy element z lewej podtablicy ktory jest wiekszy od pivota
          if (i < j)											//jesli nie przekroczylismy zakresu
         { 
                 temp=dane[i];    								//zamieniamy obie te wartosci
                 dane[i]=dane[j];								//uzywajac zmiennej pomocniczej
                 dane[j]=temp;
                 i++;
                 j--;
         }
     }while (i < j);    										//petla wykonuje sie tak dlugo, dopoki nie skoncza sie obie podtablice
	  
     return j;     												//funkcja zwraca polozenie pivota
	
}

template <typename T>
void sortuj(T dane[],int poczatek, int rozmiar, int glebokosc){
	int n=rozmiar;
	if(n<=1)
		return;
	else if(glebokosc!=0 && poczatek<rozmiar){
		T pivot=partycjonowanie(dane, 1, rozmiar);
		sortuj(dane,poczatek,pivot, glebokosc-1);
    	sortuj(dane,pivot+1,rozmiar,glebokosc-1);
	}
	else{
    	sortowanie_kopiec(dane, rozmiar);
		return;
	}
}

template <typename T>
void sortowanie_introspektywne(T dane[], int rozmiar){
	int glebokosc=2*log2(rozmiar);			//ustalenie maksymalnej rekurencji dla quicksortu
	
	sortuj(dane, 1,rozmiar,glebokosc);
}

int main(){
	int *dane;
	
	dane = new int[ROZMIAR];
	cout<<"Podaj dane: "<<endl;
	for(int i=1; i<ROZMIAR;i++)
		cin>>dane[i];
	sortowanie_introspektywne(dane, ROZMIAR-1);
	
	cout<<"Wynik: ";
	for(int j=1;j<ROZMIAR;j++)
		cout<<dane[j]<<" ";
}
